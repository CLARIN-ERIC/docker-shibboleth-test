#!/bin/bash

BINARY="server"
PROJECT_PATH="golang/src/shib-test"

init_data (){
    LOCAL=0
    if [ "$1" == "local" ]; then
        LOCAL=1
    fi

    if [ "${LOCAL}" -eq 0 ]; then
        #Remote / gitlab ci
        echo "Building ${BINARY}_linux remotely"
        cd .. || return
        docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${PROJECT_PATH}" -v "$PWD/${PROJECT_PATH}":/go/src/shib-test -w /go/src/shib-test golang:1.21 make
        mv "./${PROJECT_PATH}/${BINARY}_"* ./image && \
        cd image || return
    else
        #Local build
        cd .. || return
        echo "Building ${BINARY}_linux locally"
	    docker run --rm -e "BINARY=${BINARY}" -e "PROJECT_PATH=${PROJECT_PATH}" -v "$PWD/${PROJECT_PATH}":/go/src/shib-test -w /go/src/shib-test golang:1.21 make
        mv "./${PROJECT_PATH}/${BINARY}_"* ./image
        cd ./image || return
    fi
}

cleanup_data () {
    echo "Removing ${BINARY}_*"
    rm -f "${BINARY}_"*
}
