package commands

import (
	"github.com/spf13/cobra"
)

var ServerCmd = &cobra.Command{
	Use:   "server",
	Short: "Shibboleth test server",
	Long: `Control interface for the shibboleth test server.`,
}

func Execute() {
	var verbose bool

	ServerCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "Run in verbose mode")

	ServerCmd.AddCommand(versionCmd)
	ServerCmd.AddCommand(InitStartCommand(&verbose))
	ServerCmd.Execute()
}