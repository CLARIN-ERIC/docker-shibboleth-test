package commands

import (
	"fmt"
	"github.com/spf13/cobra"
)

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of docker-clarin",
	Long:  `All software has versions. This is docker-clarin's.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		printCheckerVersion()
		return nil
	},
}

func printCheckerVersion() {
	fmt.Printf("Shibboleth test server v%s by CLARIN ERIC\n", "1.0.0-beta")
}
