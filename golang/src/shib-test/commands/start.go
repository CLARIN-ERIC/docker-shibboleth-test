package commands

import (
	"github.com/spf13/cobra"
	"shib-test/server"
)

func InitStartCommand(verbose *bool) (*cobra.Command) {
	var port int
	var path string
	var hostname string

	var cmd = &cobra.Command{
		Use:   "start",
		Short: "start server",
		Long: `Start server`,
		Run: func(cmd *cobra.Command, args []string) {
			server.StartServerAndblock(port, hostname, path)
		},
	}

	cmd.Flags().IntVarP(&port, "port", "p", 8080, "Base port to run the server on")
	cmd.Flags().StringVarP(&path, "path", "P", "/", "Context path to deploy application on")
	cmd.Flags().StringVarP(&hostname, "hostname", "H", "catalog.clarin.eu", "Hostname to deploy application on")

	return cmd;
}
