package server

import (
	"log"
	"encoding/json"
	"fmt"
	"net/http"
)

func handlerJson(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request to %s from %s\n", r.URL.Path, r.RemoteAddr)

	result := process(r.Header)

	b, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprintf(w, "Request to %s from %s\n", r.URL.Path, r.RemoteAddr)
		fmt.Fprintf(w, "\n")
		fmt.Fprintf(w, "Failed to marshall to JSON. Error: %v\n", err)
	} else {
		w.Header().Set("Content-Type", "application/json")
		fmt.Fprintf(w, "%v\n", string(b))
	}
}
