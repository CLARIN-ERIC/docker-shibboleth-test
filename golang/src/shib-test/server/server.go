package server

import (
	"log"
	"strings"
	"net/http"
	"strconv"
)

var context_path string = ""
var hostname string = ""

func StartServerAndblock(port int, _hostname, path string) {

	context_path = path
	hostname = _hostname

	//Register handlers
	http.HandleFunc("/status", handlerJson)
	//http.HandleFunc("/", indexHandler)
	http.HandleFunc(path, handlerHtml)

	//Start server
	log.Printf("Starting http server on port: %d. Host=%s, Path=%s\n", port, hostname, path)
	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(port), nil))
}
/*
type Page struct {
	Title string
	Body  []byte
}

var indexPage *Page

func loadPage(title string) (*Page, error) {
	path := "/html"
	filename := "index.html"
	path_to_index_file := fmt.Sprintf("%s/%s", path, filename)

	fmt.Printf("Loading: %s\n", path_to_index_file)
	body, err := ioutil.ReadFile(path_to_index_file)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	var err error
	if indexPage == nil {
		indexPage, err = loadPage("Index")
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			fmt.Printf("Failed to load index. Err: %v\n", err)
		}
	}

	fmt.Printf("Request: %s\n", r.URL.Path[1:])

	if indexPage == nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Printf("Index not available.\n", err)
	} else {
		w.Header().Set("Content-Type", "text/html; charset=utf-8")
		fmt.Fprintf(w, string(indexPage.Body))
	}
*
*/

func process(header http.Header) (*Result) {
	//Defined attributes
	//See: https://www.clarin.eu/content/attributes-service-provider-federation
	attributes := Attributes{}
	attributes.AddRequiredAttribute("eduPersonTargetedID", []string{"edupersontargetedid"})
	attributes.AddRequiredAttribute("eduPersonPrincipalName", []string{"oid-edupersonprincipalname", "mace-edupersonprincipalname"})
	attributes.AddOptionalAttribute("mail", []string{"mail"})
	attributes.AddOptionalAttribute("organizationName", []string{"organizationname"})
	attributes.AddOptionalAttribute("cn", []string{"cn"})
	attributes.AddOtherAttribute("displayName", []string{"displayname"})
	attributes.AddOtherAttribute("eduPersonEntitlement", []string{"edupersonentitlement"})
	attributes.AddOtherAttribute("persistent-id", []string{"persistent-id"})
	attributes.AddOtherAttribute("eduPersonScopedAffiliation", []string{"edupersonscopedaffiliation"})

	//Process all received headers
	var result Result
	for key, value := range header {
		lkey := strings.ToLower(key)
		if isAuthUser(lkey) {
			result.AuthUser = value[0]
		} else if isAuthType(lkey) {
			result.AuthType = value[0]
		} else {
			if isShibbolethAttribute(lkey) {
				result.Shibboleth.Headers = append(result.Shibboleth.Headers, Header{key, value, })
			} else {
				is_attribute := attributes.AddValuesByKey(lkey, value)
				if !is_attribute {
					result.Http.Headers = append(result.Http.Headers, Header{key, value})
				}
			}
		}
	}
	result.Shibboleth.Required = attributes.Required
	result.Shibboleth.Optional = attributes.Optional
	result.Shibboleth.Other = attributes.Other

	return &result
}

/**
Remote_user [https://test-idp.clarin.eu!https://test-sp1.clarin.eu!39e3f3e0-96c0-40a4-b659-d2ad98589cb8]
 */
func isAuthUser(name string) (bool) {
	return name == "remote_user"
}

func isAuthType(name string) (bool) {
	return name == "auth_type"
}

func isShibbolethAttribute(name string) (bool) {
	return strings.HasPrefix(name, "shib-")
}

type Or struct {
	Attributes []*Attribute
}

func (o *Or) evaluate() (bool) {
	for _, a := range o.Attributes {
		if a.HasValue() {
			return true
		}
	}
	return false
}

func (o *Or) Add(a *Attribute) {
	o.Attributes = append(o.Attributes, a)
}

/**
 * Main and sub containers to structure response
 */
type Result struct {
	AuthUser string
	AuthType string
	Shibboleth Shibboleth
	Http Http
}

func (r *Result) GetAttribute(name string) (*Attribute) {
	for _, attr := range r.Shibboleth.Required.Attributes {
		if attr.Name == name {
			return attr
		}
	}

	for _, attr := range r.Shibboleth.Optional {
		if attr.Name == name {
			return attr
		}
	}

	for _, attr := range r.Shibboleth.Other {
		if attr.Name == name {
			return attr
		}
	}

	return nil
}

func (r *Result) GetShibHeader(name string) (string) {
	for _, attr := range r.Shibboleth.Headers {
		if attr.Name == name {
			if len(attr.Values) <= 0 {
				return ""
			} else {
				return attr.Values[0]
			}
		}
	}
	return ""
}

type Shibboleth struct {
	Headers []Header
	Required *Or
	Optional []*Attribute
	Other []*Attribute
}

type Http struct {
	Headers []Header
}

/**
 * Wrap a header with an additional type
 */

type Header struct {
	Name string
	Values []string
}

/**
 *  Attributes container holding all allowed attributes with their possible values
 */
type Attributes struct {
	Required *Or
	Optional []*Attribute
	Other []*Attribute
}

func (a *Attributes) AddRequiredAttribute(name string, keys []string) {
	if a.Required == nil {
		or := Or{}
		or.Add(&Attribute{Name: name, Keys: keys})
		a.Required = &or
	} else {
		a.Required.Add(&Attribute{Name: name, Keys: keys})
	}
}

func (a *Attributes) AddOptionalAttribute(name string, keys []string) {
	a.Optional = append(a.Optional, &Attribute{Name: name, Keys: keys})
}

func (a *Attributes) AddOtherAttribute(name string, keys []string) {
	a.Other = append(a.Other, &Attribute{Name: name, Keys: keys})
}

func (a *Attributes) AddValueByKey(key, value string) (bool) {
	for _, attr := range a.Required.Attributes {
		if attr.equalsByKey(key) {
			attr.AddValue(value)
			return true
		}
	}
	for _, attr := range a.Optional {
		if attr.equalsByKey(key) {
			attr.AddValue(value)
			return true
		}
	}
	for _, attr := range a.Other {
		if attr.equalsByKey(key) {
			attr.AddValue(value)
			return true
		}
	}
	return false
}

func (a *Attributes) AddValuesByKey(key string, values []string) bool {
	for _, attr := range a.Required.Attributes {
		if attr.equalsByKey(key) {
			for _, val := range values {
				attr.AddValue(val)
			}
			return true
		}
	}
	for _, attr := range a.Optional {
		if attr.equalsByKey(key) {
			for _, val := range values {
				attr.AddValue(val)
			}
			return true
		}
	}
	for _, attr := range a.Other {
		if attr.equalsByKey(key) {
			for _, val := range values {
				attr.AddValue(val)
			}
			return true
		}
	}
	return false
}



/**
 * Single attribute
 */

type Attribute struct {
	Name string
	Keys []string
	Values []string
}

func (a *Attribute) AddValue(value string) {
	a.Values = append(a.Values, value)
}

func (a *Attribute) HasValue() (bool) {
	return len(a.Values) > 0
}

func (a *Attribute) equalsByKey(key string) (bool) {
	for _, attr_key := range a.Keys {
		if attr_key == key {
			return true
		}
	}
	return false
}
