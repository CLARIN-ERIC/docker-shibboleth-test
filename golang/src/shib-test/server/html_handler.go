package server

import (
	"log"
	"fmt"
	"io/ioutil"
	"net/http"
	"path/filepath"
	"strings"
)

func handlerHtml(w http.ResponseWriter, r *http.Request) {
	log.Printf("Request to %s from %s\n", r.URL.Path, r.RemoteAddr)

	context_path = strings.TrimSuffix(context_path, "/")
	path := strings.TrimPrefix(r.URL.Path, context_path)

	host := r.Header.Get("Host")
	proxy_vhost := r.Header.Get("Proxy-vhost")

	fmt.Printf("Received Host: %s, Proxy-vhost: %s\n", host, proxy_vhost)

	if len(path) > 0 && path[:1] == "/" {
		relative_path := path[1:]
		if len(relative_path) == 0 {
			result := process(r.Header)
			if result == nil {
				log.Printf("Result is nil")
			} else {
				writeIndex(w, result,proxy_vhost)
			}
		} else {
			returnFile(w, relative_path)
		}
	}
}

func returnFile(w http.ResponseWriter, relative_path string) {
	path := "/html"
	path_to_index_file := fmt.Sprintf("%s/%s", path, relative_path)

	log.Printf("Loading: %s\n", path_to_index_file)
	body, err := ioutil.ReadFile(path_to_index_file)
	if err != nil {
		w.WriteHeader(http.StatusNotFound)
		log.Printf("Index not available at %s. Error: %s\n", path_to_index_file, err)
		return
	}

	if body == nil || len(body) <= 0{
		w.WriteHeader(http.StatusInternalServerError)
		log.Printf("Failed to read file at %s.\n", path_to_index_file)
		return
	}

	ext := filepath.Ext("index")
	switch ext {
	case ".css": w.Header().Set("Content-Type", "text/css")
	case ".html": w.Header().Set("Content-Type", "text/html")
	}

	fmt.Fprintf(w, string(body))
}

func writeIndex(w http.ResponseWriter, result *Result, proxy_vhost string) {
	shibSessionId := result.GetShibHeader("Shib-Session-Id")
	writeOpenHtml(w)
	if shibSessionId == "" {
		writeNoShibbolethSession(w, proxy_vhost)
	} else {
		writeShibbolethSession(w, result)
	}
	writeCloseHtml(w)
}

func writeOpenHtml(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(w, "<html>")
	fmt.Fprintf(w, "<head>")
	fmt.Fprintf(w, "<title>CLARIN SPF Interoperability Test Page</title>")
	fmt.Fprintf(w, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />")
	fmt.Fprintf(w, "<link rel=\"stylesheet\" type=\"text/css\" href=\"style.css\">")
	fmt.Fprintf(w, "</head>")
	fmt.Fprintf(w, "<body>")
	fmt.Fprintf(w, "<h1>CLARIN SPF Interoperability Test Page</h1>")
	fmt.Fprintf(w, "<p>")
}

func writeCloseHtml(w http.ResponseWriter) {
	fmt.Fprintf(w, "</body>")
	fmt.Fprintf(w, "</html>")
}

func writeNoShibbolethSession(w http.ResponseWriter, proxy_vhost string) {
	//Use the x_forwarded_for hostname if available, otherwise defaults to the configured hostname
	//TODO: use proto as well?
	use_hostname := hostname
	//	fmt.Printf("Proxy vhost = %s\n", proxy_vhost)
	if proxy_vhost != "" {
		use_hostname = proxy_vhost
	}

	//Todo:  make this location configurable
	escaped_scheme := strings.Replace("https://", ":", "%3A", -1)
	escaped_scheme = strings.Replace(escaped_scheme, "/", "%2F", -1)
	escaped_context_path := strings.Replace(context_path, "/", "%2F", -1)
	return_loc := fmt.Sprintf("%s%s%s", escaped_scheme, use_hostname, escaped_context_path,)
	fmt.Fprintf(w, "<p>No Shibboleth session exists, please <a href=\"/Shibboleth.sso/Login?target=%s&return=%s\">Login</a>.</p>", return_loc, return_loc)
}

func writeShibbolethSession(w http.ResponseWriter, result *Result) {
	err := 0
	warn := 0

	shibIdp := result.GetShibHeader("Shib-Identity-Provider")
	shibAppId := result.GetShibHeader("Shib-Application-Id")
	shibSessionId := result.GetShibHeader("Shib-Session-Id")
	eppn := result.GetAttribute("eduPersonTargetedID")
	eptid := result.GetAttribute("eduPersonPrincipalName")

	fmt.Fprintf(w, "A Shibboleth session was established with <em>%s</em><br />", shibIdp)
	fmt.Fprintf(w, "<span class=\"indent\">Shibboleth session id: <code>%s</code></span><br />", shibSessionId)
	fmt.Fprintf(w, "<span class=\"indent\">Shibboleth application id: <code>%s</code></span><br />", shibAppId)
	fmt.Fprintf(w, "<span class=\"indent\">Authentication type: <code>%s</code></span><br />", result.AuthType)
	fmt.Fprintf(w, "<span class=\"indent\">Authenticated user (REMOTE_USER): <code>%s</code></span>", result.AuthUser)
	fmt.Fprintf(w, "</p>")
	err += writeRequiredOrAttribute(w, eppn, eptid)
	warn += writeOptionalAttribute(w, result.GetAttribute("mail"))
	warn += writeOptionalAttribute(w, result.GetAttribute("organizationName"))
	warn += writeOptionalAttribute(w, result.GetAttribute("cn"))
	if err == 0 {
		fmt.Fprintf(w, "<p class=\"result ok\">Interoperability between your SP and the IDP is sufficent. %d error(s), %d warning(s)</p>", err, warn)
	} else {
		fmt.Fprintf(w, "<p class=\"result error\">Interoperability between your SP and the IDP is problematic. %d error(s), %d warning(s)</p>", err, warn)
	}
	writeHeadersTable(w, result)
}

func writeRequiredOrAttribute(w http.ResponseWriter, attr1, attr2 *Attribute) (int) {
	err := 0
	if attr1.HasValue()|| attr2.HasValue() {
		fmt.Fprintf(w, "<p class=\"attr ok\">")
		fmt.Fprintf(w, "Required attribute <code>%s</code> or <code>%s</code> is available<br />", attr1.Name, attr2.Name)
		if attr1.HasValue() {
			//TODO: support printing actual attribute key used
			fmt.Fprintf(w, "<span class=\"indent\">Attribute <code>%s</code> is available (exported as <code>%s</code>).</span><br />", attr1.Name, attr1.Keys[0])
		}
		if attr2.HasValue() {
			//TODO: support printing actual attribute key used
			fmt.Fprintf(w, "<span class=\"indent\">Attribute <code>%s</code> is available (exported as <code>%s</code>).</span><br />", attr2.Name, attr2.Keys[0])
		}
		fmt.Fprintf(w, "</p>")
	} else {
		fmt.Fprintf(w, "<p class=\"attr error\">Required attribute <code>%s</code> or <code>%s</code> is not available</p>", attr1.Name, attr2.Name)
		err += 1
	}
	return err
}

func writeOptionalAttribute(w http.ResponseWriter, attr *Attribute) (int) {
	warn := 0
	if attr.HasValue() {
		//TODO: support printing actual attribute key used
		fmt.Fprintf(w, "<p class=\"attr ok\">Optional attribute <code>%s</code> is available (exported as <code>%s</code>)</p>", attr.Name, attr.Keys[0])
	} else {
		fmt.Fprintf(w, "<p class=\"attr warn\">Optional attribute <code>%s</code> is not available</p>", attr.Name)
		warn += 1
	}
	return warn
}

func writeHeadersTable(w http.ResponseWriter, result *Result) {
	fmt.Fprintf(w, "<table class=\"attr\">")
	fmt.Fprintf(w, "<tbody>")
	fmt.Fprintf(w, "<tr class=\"header\"><th colspan=\"2\">Shibboleth headers:</th></tr>")
	for i := 0; i < len(result.Shibboleth.Headers); i++ {
		h := result.Shibboleth.Headers[i]
		writeTableRow(w, h.Name, h.Values, i)
	}
	fmt.Fprintf(w, "<tr class=\"header\"><th colspan=\"2\">Shibboleth attributes:</th></tr>")
	for i := 0; i < len(result.Shibboleth.Required.Attributes); i++ {
		a := result.Shibboleth.Required.Attributes[i]
		writeTableRow(w, a.Name, a.Values, i)
	}
	for i := 0; i < len(result.Shibboleth.Optional); i++ {
		a := result.Shibboleth.Optional[i]
		writeTableRow(w, a.Name, a.Values, i)
	}
	for i := 0; i < len(result.Shibboleth.Other); i++ {
		a := result.Shibboleth.Other[i]
		writeTableRow(w, a.Name, a.Values, i)
	}
	fmt.Fprintf(w, "<tr class=\"header\"><th colspan=\"2\">Other HTTP headers:</th></tr>")
	for i := 0; i < len(result.Http.Headers); i++ {
		h := result.Http.Headers[i]
		writeTableRow(w, h.Name, h.Values, i)
	}
	fmt.Fprintf(w, "</tbody>")
	fmt.Fprintf(w, "</table>")
}

func writeTableRow(w http.ResponseWriter, key string, values []string, rownum int) {
	class := "even"
	if rownum % 2 == 0 {
		class = "odd"
	}
	if len(values) > 0 {
		values_output := values[0]
		for i := 1; i < len(values); i++ {
			values_output = fmt.Sprintf("%s,%s", values_output, values[i])
		}
		fmt.Fprintf(w, "<tr class=\"%s\"><td>%s</td><td>%s</td></tr>", class, key, values_output)
	}
}